package info.hccis.student.dao;

import info.hccis.student.entity.Student;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Student database class for student
 *
 * @author bjmaclean
 * @since 20210924
 */
public class StudentDAO {

    private static ResultSet rs;
    private static Statement stmt = null;
    private static Connection conn = null;

    public StudentDAO() {
        String URL = "jdbc:mysql://" + "localhost" + ":3306/" + "cis2232_sample";
        try {
            conn = DriverManager.getConnection(URL, "root", "");
        } catch (Exception e) {
            System.out.println("Error");
        }

    }

    /**
     * Select all students
     *
     * @since 20210924
     * @author BJM
     */
    public ArrayList<Student> selectAll() {
        ArrayList<Student> students = null;

        //******************************************************************
        //Use the DriverManager to get a connection to our MySql database.  Note
        //that in the dependencies, we added the Java connector to MySql which 
        //will allow us to connect to a MySql database.
        //******************************************************************
        //******************************************************************
        //Create a statement object using our connection to the database.  This 
        //statement object will allow us to run sql commands against the database.
        //******************************************************************
        try {

            stmt = conn.createStatement();
            rs = stmt.executeQuery("select * from Student;");

            //******************************************************************
            //Loop through the result set using the next method.  
            //******************************************************************
            students = new ArrayList();

            while (rs.next()) {

                Student student = new Student(rs.getString("name"),
                        rs.getInt("studentId"),
                        rs.getString("program"));

                students.add(student);
            }

        } catch (SQLException e) {

            e.printStackTrace();

        } finally {

            try {
                stmt.close();
            } catch (SQLException ex) {
                System.out.println("There was an error closing");
            }
        }
        return students;
    }

    /**
     * Select a student by name
     *
     * @since 20210924
     * @author BJM
     */
    public Student selectByName(String name) {
        Student student = null;
        Statement stmt = null;
        try {
            stmt = conn.createStatement();
            String query = "select * from Student where name='" + name + "';";
            System.out.println(query);
            rs = stmt.executeQuery(query);
        } catch (SQLException ex) {
            System.out.println("Error");
            ex.printStackTrace();
        }

        try {
            while (rs.next()) {
                student = new Student(rs.getString("name"),
                        rs.getInt("studentId"),
                        rs.getString("program"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(StudentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return student;
    }

    /**
     * Insert a student
     * @since 20210924
     * @author BJM
     */
    public void insert(Student student) {
        try {
            //**************************************
            //Now can use the getters to get the info from the student object to be
            //passed into the executeUpdate method of the stmt object.
            //**************************************
            stmt = conn.createStatement();
        } catch (SQLException ex) {
            Logger.getLogger(StudentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        String sql = "INSERT INTO Student(studentId, name, program) VALUES ("
                + student.getStudentId() + ",'" + student.getName() + "','" + student.getProgram() + "')";
        System.out.println(sql);
        try {
            stmt.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(StudentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Update a student program based on their id
     * @since 20210924
     * @author BJM
     */
    public void updateProgramById(int newId, String newProgram) {
        try {
            PreparedStatement ps = conn.prepareStatement("UPDATE `student` SET program=? WHERE studentId=?");
            ps.setString(1, newProgram);
            ps.setInt(2, newId);
            ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error updating");
        }

    }

    /**
     * Select student name by their id
     * @since 20210924
     * @author BJM
     */
    
    public String selectNameById(int idToLookUp) {
        //**************************************
        //Use a procedure
        //Try a callable statement
        //**************************************
        String nameBack = null;
        System.out.println("");
        System.out.println("Try to use a callable statement to lookup a student");
        CallableStatement cstmt = null;
        try {
            String SQL = "{call getName (?, ?)}";
            cstmt = conn.prepareCall(SQL);
            cstmt.setInt(1, idToLookUp);
            cstmt.execute();
            nameBack = cstmt.getString(2);

        } catch (SQLException e) {

            System.out.println("error");
            e.printStackTrace();
        } finally {
            System.out.println("end");
        }
        return nameBack;

    }

}
