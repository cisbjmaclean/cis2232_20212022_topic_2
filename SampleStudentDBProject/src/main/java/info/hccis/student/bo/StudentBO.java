package info.hccis.student.bo;

import info.hccis.student.dao.StudentDAO;
import info.hccis.student.entity.Student;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

/**
 * Student business logic
 *
 * @author bjmaclean
 * @since 20210921
 */
public class StudentBO {

    private static ResultSet rs;
    private static Statement stmt = null;
    private static Connection conn = null;
    private StudentDAO studentDAO;

    public void run() {
        studentDAO = new StudentDAO();

        showAllStudents();

        //******************************************************************
        //Create a statement object using our connection to the database.  This 
        //statement object will allow us to run sql commands against the database.
        //******************************************************************
        lookupAStudent();

        addStudent();

    }

    /**
     * Show all students
     *
     * @since 20210927
     * @author BJM
     */
    public void showAllStudents() {
        //show all students
        for (Student current : studentDAO.selectAll()) {
            current.display();
        }

    }

    /**
     * Allow the user to lookup a student by their name
     *
     * @since 20210924
     * @author CIS2232
     */
    public void lookupAStudent() {

        System.out.println("");
        System.out.println("Lookup a student by their name");
        System.out.println("Enter name");
        Scanner input = new Scanner(System.in);
        String name = input.nextLine();
        Student student = studentDAO.selectByName(name);
        if (student != null) {
            student.display();
        }
    }

    /**
     * Add a student
     *
     * @since 20210927
     * @author BJM
     */
    public void addStudent() {
        //**************************************
        //Create a new Student
        //**************************************
        System.out.println("");
        System.out.println("Add a new Student to the database");
        Student student = new Student();
        student.getInformation();
        studentDAO.insert(student);

    }

    /**
     * Update a student's program based on their id
     *
     * @since 20210927
     * @author BJM
     */
    public void updateStudent() {
        //**************************************
        //Use a prepared statement instead of a regular statement
        //**************************************
        System.out.println("");
        System.out.println("Try to use prepared statement to update a row in the db");
        System.out.println("Enter id to update program");
        Scanner input = new Scanner(System.in);
        int newId = input.nextInt();
        input.nextLine();

        System.out.println("New program?");
        String newProgram = input.nextLine();

        studentDAO.updateProgramById(newId, newProgram);

    }

}
