package info.hccis.student.controller;

import info.hccis.student.bo.StudentBO;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Working with files.
 *
 * @author bjmaclean
 * @since 20210916
 */
public class Controller {



    public static void main(String[] args) {

        StudentBO studentBO = new StudentBO();
        studentBO.run();
        
        
    }

}
