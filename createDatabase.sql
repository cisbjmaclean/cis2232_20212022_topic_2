drop database if exists cis2232_sample;
create database cis2232_sample;
use cis2232_sample;

--
-- Database: `cis2232_sample`
--

CREATE TABLE student (
  studentId int(3) NOT NULL COMMENT 'The student id',
  name varchar(100) NOT NULL COMMENT 'Name',
  program varchar(100) DEFAULT NULL COMMENT 'Program'
) COMMENT='This table to hold student data';

INSERT INTO Student (studentId, name, program) VALUES
(1, 'Adline', 'CIS'),
(2, 'Francisco', 'CISC');
COMMIT;


DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `getName`(IN IdIn INT, OUT NameOut VARCHAR(255))
BEGIN
   SELECT name INTO NameOut
   FROM Student
   WHERE studentId = idIn;
END$$
DELIMITER ;